AddCSLuaFile()

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "wiltOS Technologies"

SWEP.HoldType = "pistol"

SWEP.ViewModel	= "models/weapons/c_toolgun.mdl"
SWEP.WorldModel = "models/weapons/w_toolgun.mdl"
SWEP.UseHands = true

SWEP.Primary.Swap           = Sound( "weapons/clipempty_rifle.wav" )
SWEP.Primary.Sound			= Sound( "NPC_CombineCamera.Click" )
SWEP.Primary.Delete1		= Sound( "Weapon_StunStick.Melee_Hit" )
SWEP.Primary.Delete			= Sound( "Weapon_StunStick.Melee_HitWorld" )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

function SWEP:SetupDataTables()

	self:NetworkVar( "Bool", 0, "IsExit" )
	self:NetworkVar( "Bool", 1, "ModifyMode" )
	self:NetworkVar( "Int", 0, "CurrentStage" )
	self:NetworkVar( "Entity", 0, "Selected" )
	self:NetworkVar( "Vector", 0, "DrawPoint" )	
end