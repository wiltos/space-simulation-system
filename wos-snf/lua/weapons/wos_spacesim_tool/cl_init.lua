AddCSLuaFile()
include( "shared.lua" )

SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true
SWEP.CSMuzzleFlashes	= true

SWEP.ViewModelFOV		= 74
SWEP.ViewModelFlip		= false

SWEP.PrintName = "wiltOS SpaceSim Tool"
SWEP.Slot = 5
SWEP.Slotpos = 5

SWEP.CurrentStageText =
{
	[1] = "PRIMARY FIRE: Place Hyperspace Zone Bottom Corner         SECONDARY FIRE: Change Hyperspace Configuration",
	[2] = "PRIMARY FIRE: Place Hyperspace Top Corner         SECONDARY FIRE: Cancel Hyperspace Placement",
}

SWEP.ModifyText =
{
	[1] = "PRIMARY FIRE: Select Nearest Hyperspace Zone",
	[2] = "PRIMARY FIRE: Select Nearest Hyperspace Zone         SECONDARY FIRE: Change Hyperspace Zone Output Position",
}

function SWEP:DrawHUD()
	if self:GetModifyMode() then
		local text = ( self:GetSelected():IsValid() and "SELECTED A WARP ZONE" ) or "NO SELECTED WARP ZONES"
		local text2 = self.ModifyText[ self:GetCurrentStage() ] or ""
		draw.SimpleText( text2 .. "          RELOAD: Delete Nearest Hyperspace Zone          USE: Change to Placement Mode", "Trebuchet24", ScrW() * 0.5, ScrH() - 120, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		draw.SimpleText( text, "Trebuchet24", ScrW() * 0.5, ScrH() - 100, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )		
	else
		local text = ( self:GetIsExit() and "HYPERSPACE EXIT" ) or "HYPERSPACE ENTRANCE"
		local text2 = self.CurrentStageText[ self:GetCurrentStage() ] or ""
		draw.SimpleText( text2 .. "          RELOAD: Delete Nearest Hyperspace Zone          USE: Change to Modify Mode", "Trebuchet24", ScrW() * 0.5, ScrH() - 120, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		draw.SimpleText( "WALK: Add or remove Hyperspace-Only prop", "Trebuchet24", ScrW() * 0.5, ScrH() - 100, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )		
		draw.SimpleText( "CURRENT CONFIGURATION: " .. text, "Trebuchet24", ScrW() * 0.5, ScrH() - 60, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end
end

function SWEP:Deploy()

	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )

	return true
	
end  

function SWEP:ShootEffects()	
	
	self.Owner:MuzzleFlash()								
	self.Owner:SetAnimation( PLAYER_ATTACK1 )	
	
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK ) 
	
end

function SWEP:PrimaryAttack()

	self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
	self.Weapon:EmitSound( self.Primary.Sound, 100, math.random(95,105) )
	self.Weapon:ShootEffects()
	
end

function SWEP:SecondaryAttack()

	self.Weapon:EmitSound( self.Primary.Swap )	
	self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
	
end