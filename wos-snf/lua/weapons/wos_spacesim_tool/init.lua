AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_init.lua" )
include( "shared.lua" )

SWEP.Weight				= 1
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.LastUse = 0
SWEP.LastDelete = 0

function SWEP:Initialize()

	self.Weapon:SetWeaponHoldType( self.HoldType )

end

function SWEP:Deploy()

	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )

	self:SetIsExit( false )
	self:SetModifyMode( false )
	self:SetCurrentStage(1)
	self:SetSelected( NULL )

	return true
	
end  

function SWEP:Think()	
	
	if self.Owner:KeyDown( IN_USE ) and self.LastUse < CurTime() then
		self:SetModifyMode( !self:GetModifyMode() )
		self.LastUse = CurTime() + 1
		self.Weapon:EmitSound( self.Primary.Swap )	
		self:SetCurrentStage(1)
		self:SetSelected( NULL )	
	elseif self.Owner:KeyReleased( IN_WALK ) and !self:GetModifyMode() then
		local tr = self.Owner:GetEyeTraceNoCursor()
		if IsValid( tr.Entity ) and tr.Entity:GetClass() != "info_spacesim_exitzone" then
			if tr.Entity:GetClass() != "wos_spacesim_prop" then
				local ent = tr.Entity
				local dummy_prop = ents.Create( "wos_spacesim_prop" )
				dummy_prop:SetModel( ent:GetModel() )
				dummy_prop:SetSkin( ent:GetSkin() )
				dummy_prop:SetMaterial( ent:GetMaterial() )
				dummy_prop:SetAngles( ent:GetAngles() )
				dummy_prop:SetPos( ent:GetPos() )
				dummy_prop:Spawn()
				ent:Remove()
			else
				tr.Entity:Remove()
			end
		end
	end
	
end

function SWEP:Reload()

	if self.LastDelete > CurTime() then return end

	local aimpos = self:GetHitPos()
	local closest
	local dist = 100000
	
	for k,v in pairs( ents.FindByClass( "info_spacesim_exitzone" ) ) do
	
		if v:GetPos():Distance( aimpos ) < dist then
		
			dist = v:GetPos():Distance( aimpos )
			closest = v
		
		end
	
	end
	
	if IsValid( closest ) then
		self.Owner:EmitSound( self.Primary.Delete )
		closest:Remove()
	end
	
	self.LastDelete = CurTime() + 1
	
end

function SWEP:Holster()

	return true

end

function SWEP:ShootEffects()	
	
	self.Owner:MuzzleFlash()								
	self.Owner:SetAnimation( PLAYER_ATTACK1 )	
	
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK ) 
	
end

function SWEP:GetHitPos()
	local look = self.Owner:EyeAngles()
	local dir = look:Forward()
	local trace = {}
	trace.start = self.Owner:GetShootPos()	
	trace.endpos = trace.start + dir * 50
	trace.filter = { self.Owner }
	trace.mask = MASK_SOLID
	local tr = util.TraceLine( trace )
	
	return tr.HitPos
end

function SWEP:PrimaryAttack()

	self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
	self.Weapon:EmitSound( self.Primary.Sound, 100, math.random(95,105) )
	self.Weapon:ShootEffects()
	
	if self:GetModifyMode() then

		local aimpos = self:GetHitPos()
		local closest
		local dist = 100000
		
		for k,v in pairs( ents.FindByClass( "info_spacesim_exitzone" ) ) do
		
			if v:GetPos():Distance( aimpos ) < dist then
			
				dist = v:GetPos():Distance( aimpos )
				closest = v
			
			end
		
		end
		
		if IsValid( closest ) then
			self.Owner:EmitSound( self.Primary.Delete1 )
			self:SetSelected( closest )
			self:SetCurrentStage( 2 )
		end
		
		return
		
	end
	
	if self:GetCurrentStage() < 2 then

		self:SetDrawPoint( self:GetHitPos() )
		self:SetCurrentStage( 2 )
	
	else

		local hitpos = self:GetHitPos()
		local orig = ( self:GetDrawPoint() + hitpos )/2
		
		local zone = ents.Create( "info_spacesim_exitzone" )
		zone:SetPos( orig )
		zone.Min = orig - self:GetDrawPoint()
		zone.Max = orig - hitpos
		zone:Spawn()
		zone:SetIsExit( self:GetIsExit() )
		//self.LinkedPos = self:GetHitPos()
		self:SetCurrentStage( 1 )	

	end
	
end

function SWEP:SecondaryAttack()

	if self:GetModifyMode() then
		if !self:GetSelected():IsValid() then return end
		self:GetSelected():SetLinkedPos( self:GetHitPos() )
		self:GetSelected().LinkedPos = true
	else
		if self:GetCurrentStage() < 2 then
			self:SetIsExit( !self:GetIsExit() ) 
			self.Weapon:EmitSound( self.Primary.Swap )	
			self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
		else
			self:SetCurrentStage( 1 )
		end
	end
	
end
