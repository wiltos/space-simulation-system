
--[[-------------------------------------------------------------------
	Space Simulation Core Functions:
		The client-side core files for the addon
			Powered by
						  _ _ _    ___  ____  
				__      _(_) | |_ / _ \/ ___| 
				\ \ /\ / / | | __| | | \___ \ 
				 \ V  V /| | | |_| |_| |___) |
				  \_/\_/ |_|_|\__|\___/|____/ 
											  
 _____         _                 _             _           
|_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___ 
  | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __|
  | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \
  |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/
                                         |___/             
----------------------------- Copyright 2018, David "King David" Wiltos ]]--[[
							  
	Lua Developer: King David
	Contact: www.wiltostech.com
		
-- Copyright 2018, David "King David" Wiltos ]]--

wOS = wOS or {}
wOS.SpaceSim = wOS.SpaceSim or {}

wOS.SpaceSim.TeleTime = 0
wOS.SpaceSim.FlashTime = 0
wOS.SpaceSim.ReturnTime = 0
wOS.SpaceSim.ZoomEnt = NULL
wOS.SpaceSim.LastPos = nil

local settings = {
	function( eang ) return eang:Up() * 90 + eang:Right()*80 - eang:Forward()*70 end,
	function( eang ) return eang:Up() * 5 - eang:Forward()*15 end,
	function( eang ) return eang:Up() * 90 + eang:Right()*80 - eang:Forward()*70 end,
}

hook.Add( "CalcView", "wOS.SpaceSim.EmulateCamera", function( ply, pos, ang )

	local viewent = ply:GetViewEntity()
	local angchange = false
	
	if CurTime() - wOS.SpaceSim.FlashTime < 0.1 then
		if not lastpos then lastpos = pos end
		viewent = wOS.SpaceSim.Entity
		angchange = true
	end

	if not IsValid( viewent ) then return end
	if viewent:GetClass() != "wos_spacesim_ship" and !angchange then return end

	if viewent.DrawVehicle then
		viewent:DrawVehicle()
	end

	local epos = viewent:GetPos()
	local eang = viewent:GetAngles()
	
	if angchange then
	
		pos = epos + eang:Up() * 150 - eang:Forward()*400
		ang = eang
		
	else
	
		local trace = util.TraceHull( {
			start = epos,
			endpos = epos + settings[2]( eang ),
			mins = Vector( -4, -4, -4 ),
			maxs = Vector( 4, 4, 4 ),
		} )

		pos = epos + settings[2]( eang )
		ang = ( viewent:GetPos() + eang:Up()*5 - pos ):Angle()
		
	end
	
	return {
		origin = pos,
		angles = ang ,
		drawviewer = false
	}	
	
end )

hook.Add( "CreateMove", "wOS.SpaceSim.DisableNonMoves", function( cmd )
	local viewent = LocalPlayer():GetViewEntity()
	if viewent:GetClass() != "wos_spacesim_ship" then return end
	cmd:RemoveKey(IN_ATTACK)
	cmd:RemoveKey(IN_ATTACK2)
	cmd:RemoveKey(IN_RELOAD)
	cmd:RemoveKey(IN_USE)
	cmd:RemoveKey(IN_JUMP)
	cmd:RemoveKey(IN_DUCK)
end )

local DotMat = Material( "sprites/light_glow02_add_noz" )
local size = 3
hook.Add( "PostDrawOpaqueRenderables", "wOS.SpaceSim.DrawToolBox", function()

	local wep = LocalPlayer():GetActiveWeapon()
	if !IsValid( wep ) then return end
	if wep:GetClass() != "wos_spacesim_tool" then return end
	
	local look = LocalPlayer():EyeAngles()
	local dir = look:Forward()
	local trace = {}
	trace.start = LocalPlayer():GetShootPos()	
	trace.endpos = trace.start + dir * 50
	trace.filter = { LocalPlayer() }
	trace.mask = MASK_SOLID
	
	local tr = util.TraceLine( trace )
	
	cam.Start3D( EyePos(), EyeAngles() )
					
		local norm = ( EyePos() - tr.HitPos ):GetNormal()
					
		render.SetMaterial( DotMat )
		render.DrawQuadEasy( tr.HitPos + norm * size, norm, size, size, color_white, 0 )
		
	cam.End3D()

	if wep:GetModifyMode() then
		if wep:GetSelected():IsValid() then
			render.DrawLine( tr.HitPos, wep:GetSelected():GetPos(), color_white, false )
		end
	else
		if wep:GetCurrentStage() > 1 then
			local orig = ( wep:GetDrawPoint() + tr.HitPos )/2
			render.DrawWireframeBox( orig, Angle( 0, 0, 0 ), orig - wep:GetDrawPoint(), orig - tr.HitPos, color_white )		
		end
	end
	
end )

local w, h = ScrW(), ScrH()

hook.Add( "RenderScreenspaceEffects", "wOS.SpaceSim.JumpToHyper", function()
	
	if CurTime() - wOS.SpaceSim.TeleTime > 2 then return end
	
	local rat = CurTime() - wOS.SpaceSim.TeleTime
	local val = rat*0.11
	DrawMaterialOverlay( "effects/strider_pinch_dudv", val )

	surface.SetDrawColor( Color( 255, 255, 255, rat*12.5 ) )
	surface.DrawRect( 0, 0, w, h )	
	
end )

hook.Add( "RenderScreenspaceEffects", "wOS.SpaceSim.ReturnFromHyper", function()
	
	if CurTime() - wOS.SpaceSim.ReturnTime > 1 then return end
	
	local val = ( 1 - ( CurTime() - wOS.SpaceSim.ReturnTime ) )*0.22
	DrawMaterialOverlay( "effects/strider_pinch_dudv", val )
	
end )

hook.Add( "HUDPaint", "wOS.SpaceSim.FlashToHyper", function()

	if CurTime() - wOS.SpaceSim.FlashTime < 0 or CurTime() - wOS.SpaceSim.FlashTime > 2 then return end
	local val = 255 * math.min( ( wOS.SpaceSim.FlashTime - CurTime() ), 2 )/2
	surface.SetDrawColor( Color( 255, 255, 255, val ) )
	surface.DrawRect( 0, 0, w, h )
	
end )
