
--[[-------------------------------------------------------------------
	Space Simulation Networking Functions:
		The client-side network files for the addon
			Powered by
						  _ _ _    ___  ____  
				__      _(_) | |_ / _ \/ ___| 
				\ \ /\ / / | | __| | | \___ \ 
				 \ V  V /| | | |_| |_| |___) |
				  \_/\_/ |_|_|\__|\___/|____/ 
											  
 _____         _                 _             _           
|_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___ 
  | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __|
  | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \
  |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/
                                         |___/             
----------------------------- Copyright 2018, David "King David" Wiltos ]]--[[
							  
	Lua Developer: King David
	Contact: www.wiltostech.com
		
-- Copyright 2018, David "King David" Wiltos ]]--

wOS = wOS or {}
wOS.SpaceSim = wOS.SpaceSim or {}
wOS.SpaceSim.Entity = NULL

net.Receive( "wOS.SpaceSim.SetCameraEntity", function()
	
	if net.ReadBool() then
		local pos = net.ReadVector()
		local ang = net.ReadAngle()
		local model = net.ReadString()
		wOS.SpaceSim.Entity = ClientsideModel( model )
		wOS.SpaceSim.Entity:SetPos( pos )
		wOS.SpaceSim.Entity:SetAngles( ang )
		wOS.SpaceSim.TeleTime = CurTime()
		wOS.SpaceSim.FlashTime = wOS.SpaceSim.TeleTime + 2
		surface.PlaySound( "ambient/levels/labs/teleport_preblast_suckin1.wav" )
		timer.Simple( 2, function()
			surface.PlaySound( "ambient/machines/teleport" .. table.Random( { 1, 3, 4 } ) .. ".wav" )
			SafeRemoveEntity( wOS.SpaceSim.Entity )
			//surface.PlaySound( "ambient/levels/labs/teleport_postblast_thunder1.wav" )
		end )
	else
		if IsValid( wOS.SpaceSim.Entity ) then wOS.SpaceSim.Entity:Remove() end
		wOS.SpaceSim.TeleTime = 0
		wOS.SpaceSim.ReturnTime = CurTime()
		wOS.SpaceSim.FlashTime = CurTime()
		surface.PlaySound( "ambient/machines/teleport" .. table.Random( { 1, 3, 4 } ) .. ".wav" )
		surface.PlaySound( "ambient/levels/labs/teleport_postblast_thunder1.wav" )
	end

end )