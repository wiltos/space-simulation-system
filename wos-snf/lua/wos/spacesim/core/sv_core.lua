
--[[-------------------------------------------------------------------
	Space Simulation Core Functions:
		The server-side core files for the addon
			Powered by
						  _ _ _    ___  ____  
				__      _(_) | |_ / _ \/ ___| 
				\ \ /\ / / | | __| | | \___ \ 
				 \ V  V /| | | |_| |_| |___) |
				  \_/\_/ |_|_|\__|\___/|____/ 
											  
 _____         _                 _             _           
|_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___ 
  | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __|
  | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \
  |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/
                                         |___/             
----------------------------- Copyright 2018, David "King David" Wiltos ]]--[[
							  
	Lua Developer: King David
	Contact: www.wiltostech.com
		
-- Copyright 2018, David "King David" Wiltos ]]--

wOS = wOS or {}
wOS.SpaceSim = wOS.SpaceSim or {}

wOS.SpaceSim.WhiteList = {
	[ "fighter_base" ] = true,
	[ "lunasflightschool_basescript" ] = true,
	[ "lunasflightschool_basescript_heli" ] = true,
	[ "shuttle_752" ] = true,
	[ "wac_pl_base" ] = true,
	//[ "wac_hc_base" ] = true,	Disabled for now, weird things happens with helicopters
}

function wOS.SpaceSim:StorePlayerData( ply, veh )
	//Star Wars Vehicles EP1 Pack Support
	ply.LastSimShip = veh

	//Star Wars Vehicles Support
	ply.WOS_SWVEHICLESAVE = ply:GetNWEntity("752_Ship", NULL )
	ply.WOS_SWVEHICLEBOOL = ply:GetNWBool( "isDriveShip", false )
	ply:SetNWEntity("752_Ship", NULL )
	ply:SetNWBool( "isDriveShip", false )
	ply.WOS_SWVEHICLESPEC = ply:GetObserverMode()

	//LFS / WAC Vehicles Support
	ply.WOS_LFSBOOL = ply:InVehicle()

end

function wOS.SpaceSim:SetPlayerData( ply, veh )
	ply:SetNWEntity("752_Ship", ply.WOS_SWVEHICLESAVE )
	ply:SetNWBool( "isDriveShip", ply.WOS_SWVEHICLEBOOL )
	ply:SetObserverMode( ply.WOS_SWVEHICLESPEC )

	if not ply.WOS_SWVEHICLEBOOL and not ply.WOS_LFSBOOL then
		ply:SetViewEntity( ply.LastSimShip )
	else
		ply:SetViewEntity( NULL )
	end

	ply.WOS_SIMDELX, ply.WOS_SIMDELY = 0, 0
end

function wOS.SpaceSim:SafeExitPlayer( ply )
	ply:SetViewEntity( NULL )
	if IsValid( ply.LastSimShip ) then
		ply.LastSimShip:Remove()
	end
end

function wOS.SpaceSim:CreateDummyShip( ply, veh )
	if not IsValid( ply ) or not IsValid( veh ) then return end
	
	local ent = ents.Create( "wos_spacesim_ship" )
	ent:SetModel( veh:GetModel() )
	ent:SetOwner( ply )
	ent:SetModelScale( 0.03, 0 )
	ent:Spawn()
	
	for _, child in pairs( veh:GetChildren() ) do
		if not IsValid( child ) then continue end
		if not child:GetClass():find( "prop" ) then continue end
		if child:GetClass():find( "prop_vehicle" ) then continue end
		local nchild = ents.Create( child:GetClass() )
		nchild:SetModel( child:GetModel() )
		nchild:SetModelScale( child:GetModelScale()*0.03, 0 )
		nchild:SetParent( ent )
		nchild:SetLocalPos( child:GetLocalPos() )
		nchild:SetLocalAngles( child:GetLocalAngles() )
		nchild:Spawn()
		ent.ChildrenList[ #ent.ChildrenList + 1 ] = nchild
	end
	
	return ent
		
end

function wOS.SpaceSim:EnterHyperSpace( ent )
	
	if not self:CanHyperSpace( ent ) then return end

	ent.InHyperSpace = true
	
	ent:StopMotionController()
	ent:SetPos( vector_origin )
	
	local phys = ent:GetPhysicsObject()
	if IsValid( phys ) then
		phys:EnableMotion( false )	
	end

end

function wOS.SpaceSim:ExitHyperSpace( ent )
	
	if not self:CanHyperSpace( ent, true ) then return end

	local phys = ent:GetPhysicsObject()
	if IsValid( phys ) then
		phys:EnableMotion( true )	
		phys:Wake()
	end
	
	ent:StartMotionController()
	
	ent.InHyperSpace = false

end

function wOS.SpaceSim:CanHyperSpace( veh, exit )
	
	if not veh.Base then return false end
	if !wOS.SpaceSim.WhiteList[ veh.Base ] then return false end

	if not exit then
		if veh.InHyperSpace then return false end
	end

	return true
	
end

function wOS.SpaceSim:LoadZones()

	MsgN( "[wOS-SpaceSim] Loading Hyperspace zone placement.." )

	if not file.Exists( "wos", "DATA" ) then file.CreateDir( "wos" ) end
	if not file.Exists( "wos/spacesim", "DATA" ) then file.CreateDir( "wos/spacesim" ) end
	if not file.Exists( "wos/spacesim/mapdata", "DATA" ) then file.CreateDir( "wos/spacesim/mapdata" ) end

	local read = file.Read( "wos/spacesim/mapdata/" .. string.lower( game.GetMap() ) .. "_json.txt", "DATA" ) 
	if not read then
		MsgN( "[wOS-SpaceSim] No Hyperspace zones found for this map!" )
		return
	end
	
	local config = util.JSONToTable( read )
	if not config then
		MsgN( "[wOS-SpaceSim] Hyperspace zone file is empty!" )
		return	
	end
	
	for k,v in pairs( config ) do
		if v[1] then
			for c,d in pairs( v ) do
				if not istable( d ) then continue end
				local ent = ents.Create( k )
				if k == "info_spacesim_exitzone" then
					ent:SetPos( d[1] )
					ent.Min = d[2]
					ent.Max = d[3]
					ent.LinkedPos = d[4]
					ent:Spawn()
					ent:SetIsExit( d[5] )
				else
					ent:SetModel( d[1] )
					ent:SetPos( d[2] )
					ent:SetAngles( d[3] )
					ent:SetSkin( d[4] )
					ent:SetMaterial( d[5] )
					ent:Spawn()
				end
			end
		end
	end	
	
	MsgN( "[wOS-SpaceSim] Successfully loaded all Hyperspace zones!" )
	
end

function wOS.SpaceSim:SaveZones()

	MsgN( "[wOS-SpaceSim] Saving Hyperspace zone placement.." )

	local enttbl = {
		info_spacesim_exitzone = {},
		wos_spacesim_prop = {},
	}
	
	for k,v in pairs( enttbl ) do
		for c,d in pairs( ents.FindByClass( k ) ) do
			if k == "info_spacesim_exitzone" then
				table.insert( enttbl[k], { d:GetPos(), d.Min, d.Max, d:GetLinkedPos(), d:GetIsExit() } )
			else
				table.insert( enttbl[k], { d:GetModel(), d:GetPos(), d:GetAngles(), d:GetSkin(), d:GetMaterial() } )
			end
		end
	end
	
	if not file.Exists( "wos", "DATA" ) then file.CreateDir( "wos" ) end
	if not file.Exists( "wos/spacesim", "DATA" ) then file.CreateDir( "wos/spacesim" ) end
	if not file.Exists( "wos/spacesim/mapdata", "DATA" ) then file.CreateDir( "wos/spacesim/mapdata" ) end

	file.Write( "wos/spacesim/mapdata/" .. string.lower( game.GetMap() ) .. "_json.txt", util.TableToJSON( enttbl ) )
	
	MsgN( "[wOS-SpaceSim] Successfully saved all Hyperspace zones!" )
	
end

-- hook.Add( "EntityFireBullets", "wOS.SpaceSim.RelocateBullet", function( ent, bullet )

-- 	local shooter = ent
-- 	if not wOS.SpaceSim:CanHyperSpace( shooter ) then 
-- 		if not wOS.SpaceSim:CanHyperSpace( shooter:GetParent() ) then
-- 			return 
-- 		end
-- 		shooter = shooter:GetParent()
-- 	end

-- 	local ply = shooter.Pilot
-- 	if not IsValid( ply ) then return end

-- 	if not IsValid( ply.DummySimShip ) then return end
	
-- 	local tr = util.TraceLine({
-- 		start = ply.DummySimShip:GetPos(),
-- 		endpos = ply.DummySimShip:GetPos()+ply.DummySimShip:GetForward()*10000,
-- 		filter = {ply.DummySimShip},
-- 	})
	
-- 	local angPos = (tr.HitPos - ply.DummySimShip:GetPos())
-- 	bullet.Src = ply.DummySimShip:GetPos()
-- 	bullet.Dir = angPos
-- 	bullet.Tracer = 1
-- 	bullet.IgnoreEntity = ply.DummySimShip
-- 	bullet.TracerName = "AirboatGunHeavyTracer"
-- 	bullet.Callback = function( att, tr, dmginfo )
-- 		print( tr.Entity, tr.HitPos )
-- 	end
	
-- 	return true
	
-- end )

hook.Add( "ShouldCollide", "wOS.SpaceSim.PreventCollide", function( ent1, ent2 )

	if not IsValid( ent1 ) or not IsValid( ent2 ) then return end
	if not ent1.Base and not ent2.Base then return end
	if not wOS.SpaceSim.WhiteList[ ent1.Base ] and not wOS.SpaceSim.WhiteList[ ent2.Base ] then return end
	if not ent1.InHyperSpace and not ent2.InHyperSpace then return end
	
	return false
	
end )

hook.Add( "ShouldCollide", "wOS.SpaceSim.PreventSimCollide", function( ent1, ent2 )

	if not IsValid( ent1 ) or not IsValid( ent2 ) then return end
	if ent1:GetClass() != "wos_spacesim_ship" and ent2:GetClass() != "wos_spacesim_ship" then return end
	
	return false
	
end )

hook.Add( "EntityRemoved", "wOS.SpaceSim.UndoHyper", function( ent )

	if ent.InHyperSpace then ent.InHyperSpace = false end
	
end )

hook.Add( "InitPostEntity", "wOS.SpaceSim.LoadZones", function()
	wOS.SpaceSim:LoadZones()
end )

hook.Add( "PostCleanupMap", "wOS.SpaceSim.LoadZones", function()
	wOS.SpaceSim:LoadZones()
end )

hook.Add("SetupPlayerVisibility", "wOS.SpaceSim.PVSAddition", function(ply, viewent)
	if not IsValid( viewent ) then return end
	if viewent:GetClass() != "wos_spacesim_ship" then return end	
	AddOriginToPVS(viewent:GetPos())
end)

concommand.Add( "wos_spacesim_startconfig", function( ply, cmd, args )

	if not ply:IsSuperAdmin() then return end
	ply:Give( "wos_spacesim_tool" )
	
end )

concommand.Add( "wos_spacesim_endconfig", function( ply, cmd, args )

	if not ply:IsSuperAdmin() then return end
	
	if ply:HasWeapon( "wos_spacesim_tool" ) then
		ply:StripWeapon( "wos_spacesim_tool" )
	end
	
	wOS.SpaceSim:SaveZones()
	
end )