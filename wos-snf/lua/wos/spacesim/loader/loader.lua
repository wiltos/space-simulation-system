
--[[-------------------------------------------------------------------
	Space Simulation Loader:
		The actual loader!!
			Powered by
						  _ _ _    ___  ____  
				__      _(_) | |_ / _ \/ ___| 
				\ \ /\ / / | | __| | | \___ \ 
				 \ V  V /| | | |_| |_| |___) |
				  \_/\_/ |_|_|\__|\___/|____/ 
											  
 _____         _                 _             _           
|_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___ 
  | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __|
  | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \
  |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/
                                         |___/             
----------------------------- Copyright 2018, David "King David" Wiltos ]]--[[
							  
	Lua Developer: King David
	Contact: www.wiltostech.com
		
-- Copyright 2018, David "King David" Wiltos ]]--

wOS = wOS or {}
wOS.SpaceSim = wOS.SpaceSim or {}

--This order may look completely stupid, and you'd ask why I wouldn't just cluster them all together
--Well, load orders are very important, and this is the best way to control it

local string = string
local file = file

local dir = "wos/spacesim"

if SERVER then
	AddCSLuaFile( dir .. "/core/cl_core.lua" )
	AddCSLuaFile( dir .. "/core/cl_net.lua" )
end

if SERVER then

	include( dir .. "/core/sv_core.lua" )
	include( dir .. "/core/sv_net.lua" )
	
else
	 
	include( dir .. "/core/cl_core.lua" )
	include( dir .. "/core/cl_net.lua" )	
	
end
