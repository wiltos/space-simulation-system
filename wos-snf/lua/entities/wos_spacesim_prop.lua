AddCSLuaFile()

ENT.Type 		= "anim"
ENT.PrintName	= "Dummy Prop"
ENT.Author		= "King David"
ENT.Contact		= ""
ENT.Category = "wiltOS Technologies"
ENT.Spawnable			= false
 
function ENT:Initialize()
    self:SetModel( self:GetModel() )
    self:DrawShadow( false )
    self:SetSolid( SOLID_OBB )
    self:SetMoveType( MOVETYPE_NONE )
    self:SetCollisionGroup( COLLISION_GROUP_DEBRIS_TRIGGER )
end

if SERVER then return end
function ENT:Draw()
    if !IsValid( LocalPlayer():GetActiveWeapon() ) or LocalPlayer():GetActiveWeapon():GetClass() != "wos_spacesim_tool" then
        local viewent = LocalPlayer():GetViewEntity()
        if not IsValid( viewent ) then return end
        if viewent:GetClass() != "wos_spacesim_ship" then return end
    end
    self:DrawModel()
end