
include('shared.lua')

function ENT:Initialize()
end

function ENT:Think()

end

function ENT:OnRemove()

end

function ENT:Draw()
	if LocalPlayer():HasWeapon( "wos_spacesim_tool" ) then
		render.DrawWireframeBox( self:GetPos(), self:GetAngles(), self:OBBMins(), self:OBBMaxs(), ( !self:GetIsExit() and Color( 0, 255, 0, 255 ) ) or Color( 255, 0, 0, 255 ), true )
		if self:GetLinkedPos() != vector_origin then
			render.DrawLine( self:GetPos(), self:GetLinkedPos(), ( !self:GetIsExit() and Color( 0, 255, 0, 255 ) ) or Color( 255, 0, 0, 255 ), false )
		end
	end
end

