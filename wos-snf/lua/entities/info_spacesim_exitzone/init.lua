AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )

ENT.Min = Vector( -1, -1, -1 )
ENT.Max = Vector( 1, 1, 1 )

function ENT:Initialize()
	
	self.Entity:SetMoveType( MOVETYPE_NONE )
	self.Entity:SetSolid( SOLID_NONE )
	
	self.Entity:SetCollisionGroup( COLLISION_GROUP_DEBRIS_TRIGGER )
	self.Entity:SetTrigger( true )
	self.Entity:SetNotSolid( true )
	self.Entity:DrawShadow( false )	
	
	self.Entity:Activate()
	self.Entity:SetCustomCollisionCheck( true )

	self.Entity:SetPos( self.Entity:GetPos() )
	
	local maxref = self.Min
	local minref = self.Max
	
	self.Entity:SetCollisionBounds( minref, maxref )
	self.Entity:PhysicsInitBox( minref, maxref )
	
	self:SetLinkedPos( self.LinkedPos or Vector( 0, 0, 0 ) )
	
end

function ENT:Think()

end 

function ENT:Touch( veh ) 

	if !self:GetIsExit() then
		if not wOS.SpaceSim:CanHyperSpace( veh ) then 
			return 
		end	
	else
		if veh:GetClass() != "wos_spacesim_ship" then 
			return 
		end
	end

	if not self.LinkedPos then return end
	
	if self:GetIsExit() then
	
		local ply = veh:GetOwner()
		if not IsValid( ply ) then return end
		if not IsValid( ply.LastSimShip ) then return end
		
		ply.LastSimShip:SetPos( self:GetLinkedPos() )
		wOS.SpaceSim:ExitHyperSpace( ply.LastSimShip )
		
		wOS.SpaceSim:SetPlayerData( ply, veh )

		net.Start( "wOS.SpaceSim.SetCameraEntity" )
			net.WriteBool( false )
		net.Send( ply )
		
		ply.DummySimShip:Remove()
		ply.DummySimShip = nil
		
	else

		//FIRST PART SWV (both variations), SECOND PART WAC, THIRD PART LFS. I LOVE DIFFERENT STANDARDS
		local ply = veh.Pilot or veh:GetNWEntity("passenger_1", NULL )
		if veh.GetDriver then
			ply = veh:GetDriver()
		end
		if not IsValid( ply ) then return end
		if not IsValid( ply:GetViewEntity() ) then return end

		if IsValid( ply.DummySimShip ) then 
			ply.DummySimShip:Remove() 
			ply.DummySimShip = nil 
		end
		
		ply.DummySimShip = wOS.SpaceSim:CreateDummyShip( ply, veh )
		ply.DummySimShip:SetPos( self:GetLinkedPos() )
		
		//Star Wars Vehicles EP1 Pack Support
		ply:SetViewEntity( ply.DummySimShip )

		wOS.SpaceSim:StorePlayerData( ply, veh )

		net.Start( "wOS.SpaceSim.SetCameraEntity" )
			net.WriteBool( true )
			net.WriteVector( veh:GetPos() )
			net.WriteAngle( veh:GetAngles() )
			net.WriteString( veh:GetModel() )
		net.Send( ply )

		wOS.SpaceSim:EnterHyperSpace( ply.LastSimShip )

		if ply.WOS_LFSBOOL then
			ply.DummySimShip:SetDummyShip( true )
		end

	end
	
end 


function ENT:OnRemove()
	
end

function ENT:UpdateTransmitState()

	return TRANSMIT_ALWAYS 

end

