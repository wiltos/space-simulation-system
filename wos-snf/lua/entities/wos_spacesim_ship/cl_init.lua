ENT.Type 		= "anim"
ENT.PrintName	= "Dummy Ship"
ENT.Author		= "King David"
ENT.Contact		= ""
ENT.Category = "wiltOS Technologies"
ENT.Spawnable			= false
ENT.RenderGroup 		= RENDERGROUP_OPAQUE

include('shared.lua')

// Need to do this for WAC/LFS because when you're in a vehicle this doesn't draw!
// Fun source engine things
function ENT:DrawVehicle()
	if not self:GetDummyShip() then return end
	if self:GetOwner() != LocalPlayer() then return end
	if not IsValid( self.ShipModel ) then
		self.ShipModel = ClientsideModel( self:GetModel() )
		self.ShipModel:SetModelScale( self:GetModelScale(), 0 )
		return
	end
    if self.ShipModel:GetModel() != self:GetModel() then
        self.ShipModel:Remove()
        return
    end
	self.ShipModel:SetPos( self:GetPos() )
	self.ShipModel:SetAngles( self:GetAngles() )
end

function ENT:OnRemove()
	if IsValid( self.ShipModel ) then
		self.ShipModel:Remove()
	end
end

function ENT:Draw()
	local viewent = LocalPlayer():GetViewEntity()
	if not IsValid( viewent ) then return end
	if viewent:GetClass() != "wos_spacesim_ship" then return end
	self.Entity:DrawModel()
end