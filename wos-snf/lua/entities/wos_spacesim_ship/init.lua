ENT.Type 		= "anim"
ENT.PrintName	= "Dummy Ship"
ENT.Author		= "King David"
ENT.Contact		= ""
ENT.Category = "wiltOS Technologies"
ENT.Spawnable			= false

AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_init.lua" )
include( 'shared.lua' )

ENT.Throttle = 0
ENT.MaxSpeed = 250
ENT.ChildrenList = {}

function ENT:Initialize()

	self:DrawShadow( false )
	self:SetMoveType( MOVETYPE_FLY )
	self:SetCollisionGroup( COLLISION_GROUP_DEBRIS_TRIGGER )
	
	local length = 1
	local width = 1
	local height = 1
	local maxref = Vector( length/2, width/2, height/2 )
	local minref = -maxref
	self:PhysicsInitBox( minref, maxref )
	self:EnableCustomCollisions( true )
	
	local phys = self:GetPhysicsObject()
	if(phys:IsValid()) then
		phys:Wake()
		phys:EnableGravity( false )
	end
	
	self:StartMotionController()
	
end

function ENT:Think()

	if not IsValid( self:GetOwner() ) or not self:GetOwner():Alive() then self:Remove() return end
	
end

function ENT:OnRemove()

	for _, ent in pairs( self.ChildrenList ) do
		if not IsValid( ent ) then continue end
		ent:Remove()
	end

	if IsValid( self:GetOwner() ) and not self:GetOwner():Alive() then
		wOS.SpaceSim:SafeExitPlayer( self:GetOwner() )
	end

end

function ENT:PhysicsSimulate( phys, deltatime )

	if not IsValid( self:GetOwner() ) then return end

	local FWD = self.Entity:GetForward()
	local UP = self:GetUp()
	local RIGHT = FWD:Cross(UP):GetNormalized()
	
	local totalvec = FWD

	if self:GetOwner():KeyDown( IN_FORWARD ) then
		self.Throttle =  math.min( self.Throttle + 4, self.MaxSpeed )
	elseif self:GetOwner():KeyDown( IN_BACK ) then
		self.Throttle = math.Approach( self.Throttle, 0, 6 )
	else
		self.Throttle = math.Approach( self.Throttle, 0, 2 )
	end

	self.FinalAng = self.FinalAng or self:GetAngles()

	local mx = self:GetOwner().WOS_SIMDELX or 0
	local my = self:GetOwner().WOS_SIMDELY or 0

	self.FinalAng.p = self.FinalAng.p + my
	self.FinalAng.y = self.FinalAng.y - mx

	//self.FinalAng:RotateAroundAxis( self.FinalAng:Up(), -mx )
	//self.FinalAng:RotateAroundAxis( self.FinalAng:Right(), -my )

	if self:GetOwner():KeyDown( IN_MOVERIGHT ) then
		self.FinalAng:RotateAroundAxis( self.FinalAng:Forward(), -1 )
	elseif self:GetOwner():KeyDown( IN_MOVELEFT ) then
		self.FinalAng:RotateAroundAxis( self.FinalAng:Forward(), 1 )
	end
	
	phys:Wake()
	
	local ShadowParams = {}
	ShadowParams.secondstoarrive = 1 // How long it takes to move to pos and rotate accordingly - only if it could move as fast as it want - damping and max speed/angular will make this invalid ( Cannot be 0! Will give errors if you do )
	ShadowParams.pos = self:GetPos() + totalvec*self.Throttle // Where you want to move to
	ShadowParams.angle = self.FinalAng  // Angle you want to move to
	ShadowParams.maxangular = 5000 //What should be the maximal angular force applied
	ShadowParams.maxangulardamp = 10000 // At which force/speed should it start damping the rotation
	ShadowParams.maxspeed = 1000000 // Maximal linear force applied
	ShadowParams.maxspeeddamp = 10000// Maximal linear force/speed before damping
	ShadowParams.dampfactor = 0.8 // The percentage it should damp the linear/angular force if it reaches it's max amount
	ShadowParams.teleportdistance = 0 // If it's further away than this it'll teleport ( Set to 0 to not teleport )
	ShadowParams.deltatime = deltatime // The deltatime it should use - just use the PhysicsSimulate one

	phys:ComputeShadowControl( ShadowParams )

	//self:GetOwner():SetEyeAngles( angle_zero )
end

hook.Add( "SetupMove", "wOS.SpaceSim.GetMouseMoves", function( ply, mv, cmd )
	if not IsValid( ply.LastSimShip ) then return end
	ply.WOS_SIMDELX, ply.WOS_SIMDELY = cmd:GetMouseX()*0.02, cmd:GetMouseY()*0.02
end )